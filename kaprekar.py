KAPREKAR_CONSTANT = 6174

def find_max_min(number):
    list_number = [a for a in str(number)]
    while len(list_number) < 4:
        list_number.append("0")
    max_number = "".join(sorted(list_number, reverse = True))
    min_number = "".join(sorted(list_number))
    return int(max_number), int(min_number)        

def kaprekar_constant(number):
    count = 0
    while int(number) != KAPREKAR_CONSTANT:
        max_number, min_number = find_max_min(number)
        count += 1
        number = max_number - min_number
    return count, number
    
if __name__ == "__main__":
    count, number = kaprekar_constant(12)
    print(count)
    print(number)

